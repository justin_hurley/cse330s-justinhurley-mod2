<!DOCTYPE html>
<html lang="en">
    <head><title>LOGOUT</title></head>
    <body>
        <?php
            session_destroy();
            header("Location: login.html");
        ?>
    </body>
</html>