<!DOCTYPE html>
<html lang="en">
    <head><Title>txt messaging</Title></head>
    <body>
    <!-- Code taken from CSE 330 Wiki under the module 2 
    PHP section-->
    <?php
        session_start();
        // Get the filename and make sure it is valid
        $filename = basename($_FILES['uploadedfile']['name']);
        echo($filename);
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename"; 
            exit;
        }
        // substring code taken from: https://stackoverflow.com/questions/2681786/how-to-get-the-last-char-of-a-string-in-php
        if(substr($filename, -1) != 't' && substr($filename, -2) != 'x' && substr($filename, -3) != 't' && substr($filename, -4) != '.'){
            echo("Not a .txt file!");
            exit;
        }
        // Get the username and make sure it is valid
        $username = $_POST['username'];
        echo($username);
        if( !preg_match('/^[\w_\-]+$/', $username) ){
            echo "Invalid username";
            exit;
        }
        $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
        //echo($_FILES['uploadedfile']['error']);
        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path)){
            header("Location: files.php");
            exit;
        }else{
            header("Location: upload_failure.html");
            exit;
        }
    ?>
    </body>
</html>