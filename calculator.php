<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Online Calculator</title>
    </head>
    <body>
        <h1>Justin's Online Calculator</h1>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <table>
                <tr>
                    <td colspan="4">Number 1:<input type="text" name="num1"></td> 
                <tr>
                    <td>Add:<br><input type="radio" name="op" value="add"></td>
                    <td>Subtract:<br><input type="radio" name="op" value="sub"></td>
                    <td> Multiply:<br><input type="radio" name="op" value="mul"></td>
                    <td>Divide:<br><input type="radio" name="op" value="div"></td>
                </tr>
                <tr>
                    <td colspan="4">Number 2:<input type="text" name="num2"></td> 
                </tr>
            </table>
            Submit:
            <input type="submit">
        </form>
        <h2>Answer:</h2>
        <?php 
            $num1 = htmlentities($_POST['num1']);
            $num2 = htmlentities($_POST['num2']);
            $op = htmlentities($_POST['op']);
            switch($op){
                case("add"):
                    printf($num1 + $num2);
                    break;
                case("sub"):
                    printf($num1 - $num2);
                    break;
                case("mul"):
                    printf($num1 * $num2);
                    break;
                case("div"):
                    if($num2 == 0){
                        printf("Undefined");
                    }
                    else{
                        printf($num1 / $num2);
                    }
                    break;
                default:
                    printf("You need to select an operation!");
                    break;
            }
        ?>
    </body>
</html>