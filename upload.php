<!DOCTYPE html>
<html lang="en">
    <head><Title>Uploading File</Title></head>
    <body>
    <!-- Code taken from CSE 330 Wiki under the module 2 
    PHP section-->
    <?php
        session_start();
        // Get the filename and make sure it is valid
        $filename = basename($_FILES['uploadedfile']['name']);
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename"; 
            exit;
        }
        // Get the username and make sure it is valid
        $username = $_SESSION['username'];
        if( !preg_match('/^[\w_\-]+$/', $username) ){
            echo "Invalid username";
            exit;
        }
        $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
        //echo($_FILES['uploadedfile']['error']);
        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path)){
            header("Location: files.php");
            exit;
        }else{
            header("Location: upload_failure.html");
            exit;
        }
    ?>
    </body>
</html>