<!DOCTYPE html>
<html lang="en">
    <head><title>File View</title></head>
    <body>
    <!-- Code taken from CSE 330 Wiki under the module 2 
    PHP section-->
    <?php
        session_start();
        $filename = $_GET['file'];
        //checks for valid file name
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename";
            exit;
        }
        //checks for valid username
        $username = $_SESSION['username'];
        if( !preg_match('/^[\w_\-]+$/', $username) ){
            echo "Invalid username";
            exit;
        }

        $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

        // Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($full_path);

        // Finally, set the Content-Type header to the MIME type of the file, and display the file.
        header("Content-Type: ".$mime);
        readfile($full_path);
        ?>
    </body>
</html>
