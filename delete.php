<!DOCTYPE html>
<html lang="en">
    <head><title>File View</title></head>
    <body>
    <!-- Code taken from CSE 330 Wiki under the module 2 
    PHP section-->
    <?php
        session_start();
        $filename = $_GET['file'];
        echo($_GET['file']);
        //checks for valid file name
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename";
            exit;
        }
        //checks for valid username
        $username = $_SESSION['username'];
        if( !preg_match('/^[\w_\-]+$/', $username) ){
            echo "Invalid username";
            exit;
        }

        $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

        if(unlink($full_path)){
            header("Location: files.php");
            exit;
        }
        else{
            header("Location: delete_failure.html");
            exit;
        }
        ?>
    </body>
</html>
