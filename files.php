<!DOCTYPE html>
<html lang="en">
    <head>
        <title>File Sharing</title>
    </head>
    <body>
        <h1>Hello, <?php
            session_start();
            $name = $_SESSION['username'];
            echo($name);
        ?></h1>
        <?php
            session_start();
            $username = $_SESSION['username'];
        // code taken from Stack Exchange: https://stackoverflow.com/questions/15774669/list-all-files-in-one-directory-php
            $full_path = sprintf("/srv/uploads/%s", $username);
            $files = array_diff(scandir($full_path, 1), array('.','..'));
            $string = "";
            for($i = 0; $i < count($files); $i++){
                $string .= $files[$i]."<br>"; 
            }
            echo("<b>Files this User Has:</b><br>");
            echo($string);
        ?>
        <form action="get.php" method="GET">
            <p>
                <label>Enter the name of a file to view:</label>
                <input type="text" name="file">
            </p>
            <p>
                <input type="submit">
            </p>
        </form>
        <!-- Form code taken from CSE 330 Wiki under the module 2 
    PHP section-->
        <form enctype="multipart/form-data" action="upload.php" method="POST">
            <p>
                <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
                <label>Choose a file to upload:</label> <input name="uploadedfile" type="file"/>
            </p>
            <p>
                <input type="submit" value="Upload File" />
            </p>
        </form>
        <form action="delete.php" method="GET">
            <p>
                <label>Enter the name of the file to delete:</label>
                <input type="text" name="file">
            </p>
            <p>
                <input type="submit">
            </p>
        </form>
        <form enctype="multipart/form-data" action="message.php" method="POST">
            <p>
                <label>Enter the username of the person you would like to message</label>
                <input type="text" name="username"> <br>
                <label>Upload a .txt file of the message you would like to send once a username is entered</label>
                <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
                <input name="uploadedfile" type="file"/>
            </p>
            <p>
                <input type="submit" value="Send message" />
            </p>
        </form>    
        <form action="logout.php" method="GET">
            <input type="submit" value="Logout">
        </form>
        
    </body>
</html>